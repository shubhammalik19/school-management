<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model('Role_model','rm');
    }
	public function index()
	{
		$this->load->view('role');
	}
	function saveRole(){
		$roleName = $this->input->post('role');
		$role = array('ROLL_NAME'=>$roleName,
					 'CREATED_DATE'=>date('y-m-d') 	
		             );
	  if($this->rm->IsRoleExist($roleName)){
		$save = $this->db->insert('roll-master',$role);
		if($save)
			echo "Successfully Saved";
	  }
	  else{
	  	echo "FAILED TO SAVE";
	  }
	}
	function IsRoleExist(){
		$boll = $this->rm->IsRoleExist();
		if($boll)
			echo "true";
		else
			echo "flase";
	}
}

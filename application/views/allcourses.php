<?php require('header.php'); ?>
<div class="container">
	<div class="alert alert-dismissible alert-success">
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	  <h5 class="alert-heading">Message</h5>
	  <p class="mb-0">Best check yo self, you're not looking too good. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna</p>
	</div>	
</div>
<div class="container table-responsive">     	
 <div class="row" style="margin: 1px">
  <div class="col-sm-10"><h2>ALL Courses</h2></div>
  <div class="col-sm-2">
  	<button type="button" style="margin-left: 50px" class="btn btn-primary"data-toggle="modal" data-target="#add-course">ADD COURSE</button>
  </div>
 </div> 
 <div class="width1100">	     
  <table  class="table table-striped table-bordered" cellspacing="0" width="100%" id="cousrse-table">
    <thead>
      <tr>
        <th>S.No</th>
        <th>Course Name</th>
        <th>Duration</th>
        <th>Fee</th>
      </tr>
    </thead>
    <tbody>
     
    </tbody>
  </table>
</div>
</div>

 <div class="modal" id="add-course">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add Course</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form>
			  <div class="form-group row">
			    <label for="inputEmail3" class="col-sm-2 col-form-label">Course Name</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" placeholder="Course Name">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label for="inputPassword3" class="col-sm-2 col-form-label">Duration</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="inputPassword3" placeholder="Duration">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label for="inputPassword3" class="col-sm-2 col-form-label">Fee</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control"  placeholder="Fee">
			    </div>
			  </div>	
        </div>
	        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Add</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  
</div>

<?php require('footer.php'); ?>
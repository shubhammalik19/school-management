<?php require('header.php'); ?>
<div class="container">
	<div class="row grey">
		<div class="col-sm-12"><span>MANAGE EMAIL</span></div>
	</div>
	<div class="row border-1px">
		<div class="col-sm-12 padding">
			 <form>
			  <div class="form-group row">
			    <label  class="col-sm-2 col-form-label">EMAIL</label>
			    <div class="col-sm-10">
			      <input type="email" class="form-control" placeholder="EMAIL">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label  class="col-sm-2 col-form-label">EMAIL TO</label>
			    <div class="col-sm-10">
			      <input type="email" class="form-control" placeholder="EMAIL TO">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label  class="col-sm-2 col-form-label">CC</label>
			    <div class="col-sm-10">
			      <input type="email" class="form-control" placeholder="CC">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label  class="col-sm-2 col-form-label">BCC</label>
			    <div class="col-sm-10">
			      <input type="email" class="form-control" placeholder="BCC">
			    </div>
			  </div>
			   <div class="form-group row">
			    <label class="col-sm-2 col-form-label">MODULE</label>
			    <div class="col-sm-10">
			    	<select class="form-control">
				        <option selected>--SELECT--</option>
				        <option>ACCOUNTS</option>
				        <option>ENQUIRY</option>
				        <option>ADDMINSSION</option>
				        <option>INVENTORY</option>
				        <option>ADMIN</option>
				        <option>STUDENT</option>
				    </select>
			    </div>
			  </div>

			  <div class="form-group row">
			    <label  class="col-sm-2 col-form-label"></label>
			    <div class="col-sm-10">
			      <button type="submit" class="btn btn-primary">Add</button>
			    </div>
			  </div>
			
			</form>
		</div>
	</div>
</div>
<?php require('footer.php'); ?>
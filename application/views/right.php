<?php require('header.php'); ?>
<div class="container">
	<div class="row grey">
		<div class="col-sm-12"><span>MANAGE RIGHTS</span></div>
	</div>
	<div class="row border-1px">
		<div class="col-sm-6 col-12 padding">
			<form class="form-inline container">
			   <div class="form-group row">
				    <label class="col-sm-4 col-12">RIGHTS</label>
				    <select class="form-control col-sm-8 col-12">
				      <option>SELECT USER</option>
				      <option>ADMIN</option>
				    </select>
				</div>
			</form>
		</div>
		<div class="col-sm-6 col-12"></div>
	</div>
</div>
<?php require('footer.php'); ?>
<?php require('header.php'); ?>
<div class="container">
	<div class="alert alert-dismissible alert-success margin-top">
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	  <h5 class="alert-heading">Message</h5>
	  <p class="mb-0" id="message"></p>
	</div>	
</div>
<div class="container table-responsive">     	
 <div class="row" style="margin: 1px">
  <div class="col-sm-10"><h2>MANAGE ROLE</h2></div>
  <div class="col-sm-2">
  	<button type="button" style="margin-left: 50px" class="btn btn-primary"data-toggle="modal" data-target="#add-role">ADD ROLE</button>
  </div>
 </div> 	     
  <table class="table table-striped table-hover" id="role-table">
    <thead>
      <tr>
        <th>S.No</th>
        <th>ROLL NAME</th>
        <th>ACTION</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>ADMIN</td>
        <td>
          <button type="submit" class="btn btn-primary">UPDATE</button>
          <label class="switch">
    			  <input type="checkbox" checked>
    			  <span class="slider round"></span>
    			</label>
        </td>
      </tr>
    </tbody>
  </table>
</div>

 <div class="modal" id="add-role">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">ADD ROLE</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        <?=form_open('role/saveRole',['id'=>'save-role'])?>
			  <div class="form-group row">
			    <label for="inputEmail3" class="col-sm-2 col-form-label">ROLE Name</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control uppercase" placeholder="ROLE NAME" name="role">
			    </div>
			  </div>	
        </div>
	        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Add</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  
</div>

<?php require('footer.php'); ?>
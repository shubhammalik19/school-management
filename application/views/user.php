<?php require('header.php'); ?>
<div class="container">
	<div class="alert alert-dismissible alert-success">
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	  <h5 class="alert-heading">Message</h5>
	  <p class="mb-0">Best check yo self, you're not looking too good. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna</p>
	</div>	
</div>
<div class="container table-responsive">     	
 <div class="row" style="margin: 1px">
  <div class="col-sm-10"><h2>ALL Courses</h2></div>
  <div class="col-sm-2">
  	<button type="button" style="margin-left: 50px" class="btn btn-primary"data-toggle="modal" data-target="#add-user">ADD USER</button>
  </div>
 </div> 	     
  <table class="table table-striped table-hover" id="user-table">
    <thead>
      <tr>
        <th>S.No</th>
        <th>ACCESS LEVEL</th>
        <th>FACILITY ID</th>
        <th>USER ID</th>
        <th>USER NAMES</th>
        <th>PHONE NO</th>
        <th>EMAIL</th>
        <th>PASSWORD</th>
        <th>CONFIRM PASSWORD</th>
        <th>ACTION</th>
      </tr>
    </thead>
    <tbody>
      <tr>
         <td>1</td>
        <td>ACCESS LEVEL</td>
        <td>FACILITY ID</td>
        <td>USER ID</td>
        <td>USER NAMES</td>
        <td>PHONE NO</td>
        <td>EMAIL</td>
        <td>PASSWORD</td>
        <td>CONFIRM PASSWORD</th>
        <td>
        	<button type="button" class="btn btn-primary">DELET</button> 
        </td>
      </tr>
    </tbody>
  </table>
</div>

 <div class="modal" id="add-user">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add Course</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
         <form>
			  <div class="form-group row">
			    <label class="col-sm-2 col-form-label">ACCESS LEVEL</label>
			    <div class="col-sm-10">
			    	<select class="form-control">
				        <option selected>--SELECT--</option>
				        <option>...</option>
				    </select>
			    </div>
			  </div>
			  <div class="form-group row">
			    <label class="col-sm-2 col-form-label">FACILITY ID</label>
			  <div class="col-sm-10">
			    	<select class="form-control">
				        <option selected>--SELECT--</option>
				        <option>...</option>
				    </select>
			    </div>
			  </div>
			  <div class="form-group row">
			    <label  class="col-sm-2 col-form-label">USER ID</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" placeholder="USER ID">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label  class="col-sm-2 col-form-label">Password</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" placeholder="Password">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label  class="col-sm-2 col-form-label">USER NAME</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" placeholder="USER NAME">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label  class="col-sm-2 col-form-label">PHONE NUMBER</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" placeholder="PHONE NUMBER">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label  class="col-sm-2 col-form-label">EMAIL</label>
			    <div class="col-sm-10">
			      <input type="email" class="form-control" placeholder="EMAIL">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label  class="col-sm-2 col-form-label">Password</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" placeholder="Password">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label  class="col-sm-2 col-form-label">CONFIRM Password</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" placeholder="CONFIRM Password">
			    </div>
			  </div>
	        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Add</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  
</div>

<?php require('footer.php'); ?>
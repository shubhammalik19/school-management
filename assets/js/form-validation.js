$(document).ready(function(){
	jQuery.validator.addMethod("alphanumeric", function(value, element) {
	    return this.optional(element) || /^[a-zA-Z0-9áéíóúÁÉÍÓÚÑñ ]+$/.test(value);
	},"Only Numbers and Charecter allowed");

	$("#save-role").validate({
		rules:{
				role:{
					required:true,
					minlength:5,
					maxlength:50,
					alphanumeric: true	
				}
		},
		submitHandler:function(){
			$("#save-role").saveFormAjax(site_url('role/saveRole'));
	        return false;
		}
	});
})
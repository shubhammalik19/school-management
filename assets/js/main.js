function  site_url(url) {
   let base_url = "http://localhost/iant/"+url;
   return base_url;
}
function  base_url() {
   let base_url = "http://localhost/iant/";
   return base_url;	
}
$(".uppercase").on("keyup",function(){
	let val = $(this).val().toUpperCase();
	$(this).val(val);
});
(function ( $ ) { 	
 	$.fn.saveFormAjax = function(server){
 		let data = $(this).serializeArray();
 		let dataObj = {};

 		$.each(data, function(i,feild){
          dataObj[feild.name] = feild.value;
       });
 		console.log(dataObj);
 		$.ajax({
 				url:server,
 				type:"post",
 				data:dataObj,
 				success:function(result){
 					$("#message").text(result);
 					if(result=="Successfully Saved"){
 					   $('form')[0].reset();
 					}
 				}
 		});
 		return this;
 	}
 	// server function name , names array of colume in same order as table heading colums ,in array for exporting 'copy', 'csv', 'excel', 'pdf', 'print'
 									//string, array , array
 	$.fn.getDataTableAjax = function(server,columns,expButton){
 		let col = [];
 		for(let i=0;i<columns.length;i++)
 		   col[i] = {"data":columns[i]};
 		let thisObj = $(this);
 		$.ajax({
 				url:server,
 				dataType: "JSON",
 				type:"post",
 				success:function(result){
 					thisObj.DataTable({
 						dom: 'Bfrtip',
				        buttons: expButton,
			 			data:result,
			 			columns:col
			 		});
 				}
 		});	
 		return this;
 	}
 	//convert upto 2 decimal places
 	$.fn.upToDecimal = function(){
	 	return this.each(function(){
		 	 $(this).on("blur",function(){
		 	 	let val = Number($(this).val()).toFixed(2);
		 	 	$(this).val(val);
		 	 })
	 	});					
 	}
 	//create timer  
 	$.fn.crateTimer = function(minuts){
 		return this.each(function(){
 			let t = setInterval(setTimeCouter,1000);
		 	let mint=minuts - 1;
			let sec=60;let obj = $(this);
			function setTimeCouter()
			{
				sec--;
				if(mint==0 && sec==0)
				{
					clearInterval(t);
				}
				else
				if(sec==0)
				{
					sec=60;
					mint--;
				}
				$(obj).text(mint+":"+sec);
			}
 		});
 	}
                   
   // get exam questions from data base - time per question
    $.fn.createExamPaper = function(timePerQes,server){
		return this.each(function() {
			let exmStructure = 
			`<div class="hide-show"> 
				    <fieldset class="form-group">
				     <div class="row"> 
				      <legend class="col-sm-12" id="question">Radio buttons </legend>
				     </div> 
				  
				    <div class="row padding15">  
					      <div class="form-check col-sm-6">
					        <label class="form-check-label">
					          <input class="form-check-input" name="options" id="options1" type="radio">
					          <span class='optionsLable'>option 1</span>
					        </label>
					      </div>

					       <div class="form-check col-sm-6">
					        <label class="form-check-label">
					          <input class="form-check-input" name="options" id="options2" type="radio">
					          <span class='optionsLable'>option 2</span>
					        </label>
					      </div>
				    </div>
					
					 <div class="row padding15">  
					      <div class="form-check col-sm-6">
					        <label class="form-check-label">
					          <input class="form-check-input" name="options" id="options3" type="radio">
					          <span class='optionsLable'>option 3</span>
					   	     </label>
					      </div>

					       <div class="form-check col-sm-6">
					        <label class="form-check-label">
					          <input class="form-check-input" name="options" id="options4" type="radio">
					          <span class='optionsLable'>option 4</span>
					        </label>
					      </div>  
				    </div>

				    </fieldset>
				    <div class="row padding15"> 
				      <button type="button" class="btn btn-primary col-sm-2 marginRight next">Submit</button>
				      <div class="col-sm-8"></div>
				    </div> 
				  </div>`;
				  $(this).html(exmStructure);
			      let index = 0;
			     $.ajax({
 				           url:server,
			 				type:"post",
			 				dataType: "JSON",
			 				success:function(result){
			 					setInterval(function(){
			 						showNextQuestion();
			 					},timePerQes*1000*60)
			 					function showNextQuestion(){
			 						let obj = result[index];
			 						$("#question").text(obj.question)
			 						$(".optionsLable").each(function(i) {
			 							$(this).text(obj[`option${i+1}`]);
			 						});
			 						$('input[name=options]:checked').val();
			 						index++;
			 					}
			 					showNextQuestion();
			 				}
			 		});
		});
    }

}( jQuery ));

$("#fee").upToDecimal();	
$(document).ready(function(){	
	$("#cousrse-table").getDataTableAjax("ajax.php",["Id","name","duration","fee"],['copy', 'csv', 'excel', 'pdf', 'print']);
})
$("#timer").crateTimer(2);
$("#exam").createExamPaper(2,"http://localhost/iant/exam/question");
/*$(document).ready(function(){
	$(".hide-show").hide();
	$(".hide-show").eq(0).show();
	$('.next').on('click', function(event) {
		let obj = $(this).parent("div").parent("div");
		obj.hide();                                                                                                                                                                                                                                  
		obj.next("div.hide-show").show();
	});
})*/